# Vue 3 Fullscreen Image Directive Plugin

A lightweight Vue.js plugin for creating a fullscreen image modal with customizable options, that support panorama display.

## Features

- **Lightweight:** Our package is designed to be minimalistic and efficient, ensuring a small footprint without compromising functionality.

- **Panorama Support:** Now supports panorama images! Pass an array of `imageUrl` to create a panorama view.

- **Accessible:** We prioritize accessibility to ensure that our package can be easily used by everyone, including users with disabilities.

- **Abundant Options:** With a plethora of customizable options, you have the flexibility to tailor the package to suit your specific needs. Explore the extensive set of features to enhance your development experience.

- **Responsive:** Our package is built to be responsive, adapting seamlessly to various screen sizes and devices.


## Installation

Install the package using npm:

```bash
npm install vue-3-fullscreen-image-directive-plugin
yarn add vue-3-fullscreen-image-directive-plugin
```

## Usage

### Register the Plugin

```typescript
import { createApp } from 'vue'
import App from './App.vue'
import { fullscreenImagePlugin } from './fullscreen-image-plugin'
import 'vue-3-fullscreen-image-directive-plugin/style.css' // Don't forget to import the style

createApp(App).use(fullscreenImagePlugin).mount('#app')
```

### Use the Directive
Add the v-fullscreen-image directive to any element you want to trigger the fullscreen image modal on click.

#### Single Image 

![Demo single](https://i.imgur.com/pTAguLR.gif)

```vue
<template>
  <img v-fullscreen-image="{
    imageUrl: 'https://placehold.co/2000x4000',
    withDownload: false,
    animation: 'blur'
  }"
    src="https://placehold.co/200x400"
  />
</template>
```

#### Panorama Images

![Demo panorama](https://i.imgur.com/h8Z9jNQ.gif)

```vue
<template>
  <img v-fullscreen-image="{
    imageUrl: 'https://placehold.co/2000x4000',
    withDownload: true,
    animation: 'blur',
    gallery: 'my-image-group'
  }"
    src="https://placehold.co/200x400"
  />
  <img v-fullscreen-image="{
    imageUrl: 'https://placehold.co/1100x1100',
    withDownload: true,
    animation: 'blur',
    gallery: 'my-image-group'
  }"
    src="https://placehold.co/200x400"
  />
</template>
```

### Directive Options

|Name   |Type  | Description  |
|---|---|---|
|imageUrl  | (string, required)   | The URL of the image to display  |
|anchor | 	(string, optional, default: 'body') | Selector for the element to which the modal will be mounted. |
|animation  | (string, optional, default: 'fade')   | Animation type ('fade', 'blur', 'none').  |
|imageAlt  | (string, optional)   | Alt text for the image.  |
|withDownload  | (boolean, optional, default: true)   | Show the download button.  |
|withClose  | (boolean, optional, default: true)   | Show the close button.  |
|withFocusOnClose  | (boolean, optional, default: true)   | Automatically focus on the close button when the modal opens.  |
|withCloseOnEscape  |  (boolean, optional, default: true)   | Close the modal when the 'Escape' key is pressed.  |
|closeOnClickOutside  |  (boolean, optional, default: true)   | Close the modal when clicking outside the image.  |
|maxHeight  | (string, optional, default: '80vh')   | Maximum height of the image container.  |
|maxWidth  | (string, optional, default: '80vw')   | Maximum width of the image container.  |
|backdropColor  | (string, optional, default: 'rgba(0, 0, 0, 0.7)')   | Color of the modal backdrop.  |
|gallery  | (string or boolean, optional, default: false)   | Allows navigation between images of the same "gallery" group.  |

## Author

This npm package is developed and maintained by Anatole Acqueberge, feel free to contibute.

## License

This project is licensed under the MIT License 