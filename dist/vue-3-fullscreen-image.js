import { defineComponent as T, useCssVars as j, computed as f, ref as B, onMounted as N, openBlock as i, createBlock as E, Transition as K, withCtx as O, createElementBlock as g, createElementVNode as n, createCommentVNode as I, TransitionGroup as M, normalizeClass as F, Fragment as L, renderList as S, pushScopeId as H, popScopeId as V, reactive as Y, createApp as q, h as z } from "vue";
const b = (e) => (H("data-v-6d302276"), e = e(), V(), e), W = { class: "icons" }, J = /* @__PURE__ */ b(() => /* @__PURE__ */ n("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ n("path", { d: "M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" }),
  /* @__PURE__ */ n("polyline", { points: "17 8 12 3 7 8" }),
  /* @__PURE__ */ n("line", {
    x1: "12",
    y1: "3",
    x2: "12",
    y2: "15"
  })
], -1)), Q = [
  J
], X = /* @__PURE__ */ b(() => /* @__PURE__ */ n("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ n("line", {
    x1: "18",
    y1: "6",
    x2: "6",
    y2: "18"
  }),
  /* @__PURE__ */ n("line", {
    x1: "6",
    y1: "6",
    x2: "18",
    y2: "18"
  })
], -1)), Z = [
  X
], $ = ["disabled"], ee = /* @__PURE__ */ b(() => /* @__PURE__ */ n("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ n("polyline", { points: "15 18 9 12 15 6" })
], -1)), oe = [
  ee
], te = ["src", "alt"], ne = ["disabled"], le = /* @__PURE__ */ b(() => /* @__PURE__ */ n("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ n("polyline", { points: "9 6 15 12 9 18" })
], -1)), ae = [
  le
], re = {
  key: 1,
  class: "image-container"
}, se = ["src", "alt"], ie = /* @__PURE__ */ T({
  __name: "FullscreenImage",
  props: {
    currentGallery: {},
    imageUrl: {},
    imageAlt: { default: "" },
    anchor: {},
    animation: { default: "fade" },
    withDownload: { type: Boolean, default: !0 },
    withClose: { type: Boolean, default: !0 },
    withFocusOnClose: { type: Boolean, default: !0 },
    withCloseOnEscape: { type: Boolean, default: !0 },
    closeOnClikOutside: { type: Boolean, default: !0 },
    maxHeight: { default: "80vh" },
    maxWidth: { default: "80vw" },
    backdropColor: { default: "rgba(0, 0, 0, 0.7)" },
    gallery: { type: [String, Boolean], default: !1 }
  },
  emits: ["close", "previous", "next"],
  setup(e, { emit: o }) {
    j((l) => ({
      ea7c8d66: l.backdropColor,
      "2513f44b": l.maxHeight
    }));
    const t = e, s = o, a = f(() => {
      var l;
      return ((l = t.currentGallery) == null ? void 0 : l.images) ?? [t];
    }), c = f(() => a.value.length), d = f(() => {
      var l;
      return ((l = t.currentGallery) == null ? void 0 : l.currentIndex) ?? 0;
    }), r = f(() => a.value[d.value]), m = B(!1), y = () => {
      m.value = !0, setTimeout(() => {
        s("close");
      }, 500);
    }, u = async (l) => {
      l.preventDefault(), l.stopPropagation();
      try {
        const _ = r.value.imageUrl, x = await (await fetch(_)).blob(), p = document.createElement("a");
        p.href = window.URL.createObjectURL(x), p.download = "image", p.style.display = "none", document.body.appendChild(p), p.click(), document.body.removeChild(p), window.URL.revokeObjectURL(p.href);
      } catch (_) {
        console.error("Error downloading image:", _);
      }
    }, h = (l) => {
      t.withCloseOnEscape && l.key === "Escape" && y();
    }, w = () => {
      t.closeOnClikOutside && y();
    }, k = f(() => d.value === 0), U = f(() => d.value >= c.value - 1), P = () => s("previous"), R = () => s("next"), C = B();
    return N(() => {
      t.withFocusOnClose && C.value && C.value.focus();
    }), (l, _) => (i(), E(K, {
      name: l.animation,
      appear: ""
    }, {
      default: O(() => [
        r.value.imageUrl && !m.value ? (i(), g("div", {
          key: 0,
          class: "fullscreen-image",
          onKeydown: h,
          tabindex: "0",
          role: "dialog",
          "aria-modal": "true",
          "aria-label": "Fullscreen Image"
        }, [
          n("div", {
            class: "backdrop",
            onClick: w
          }, [
            n("div", W, [
              r.value.withDownload ? (i(), g("button", {
                key: 0,
                tabindex: "0",
                class: "icon download-icon",
                onClick: u,
                title: "close"
              }, Q)) : I("", !0),
              r.value.withClose ? (i(), g("button", {
                key: 1,
                ref_key: "closeButtonRef",
                ref: C,
                tabindex: "0",
                class: "icon close-icon",
                onClick: y,
                title: "download"
              }, Z, 512)) : I("", !0)
            ])
          ]),
          c.value > 1 ? (i(), E(M, {
            key: 0,
            name: "list",
            tag: "div",
            class: "image-container"
          }, {
            default: O(() => [
              n("button", {
                key: "previouus",
                disabled: k.value,
                class: F(["icon", k.value && "icon--disabled"]),
                onClick: P
              }, oe, 10, $),
              (i(!0), g(L, null, S(a.value, (A, x) => (i(), g(L, {
                key: A.imageUrl + x
              }, [
                d.value === x ? (i(), g("img", {
                  key: 0,
                  src: A.imageUrl,
                  alt: r.value.imageAlt
                }, null, 8, te)) : I("", !0)
              ], 64))), 128)),
              n("button", {
                key: "neeext",
                disabled: U.value,
                class: F(["icon", U.value && "icon--disabled"]),
                onClick: R
              }, ae, 10, ne)
            ]),
            _: 1
          })) : (i(), g("div", re, [
            n("img", {
              src: r.value.imageUrl,
              alt: r.value.imageAlt
            }, null, 8, se)
          ]))
        ], 32)) : I("", !0)
      ]),
      _: 1
    }, 8, ["name"]));
  }
}), ce = (e, o) => {
  const t = e.__vccOpts || e;
  for (const [s, a] of o)
    t[s] = a;
  return t;
}, ue = /* @__PURE__ */ ce(ie, [["__scopeId", "data-v-6d302276"]]), G = "_global_gallery", v = Y({}), D = (e) => Array.isArray(e.imageUrl) ? G : (e.gallery ?? !1) === !1 ? !1 : typeof e.gallery == "string" ? e.gallery : G, de = (e) => {
  const o = D(e);
  if (o) {
    if (!(v[o] ?? !1)) {
      const t = {
        currentIndex: 0,
        images: []
      };
      v[o] = t;
    }
    if (Array.isArray(e.imageUrl))
      e.imageUrl.map((t, s) => {
        const a = {
          ...e,
          imageUrl: t,
          imageAlt: e.imageAlt && Array.isArray(e.imageAlt) ? e.imageAlt[s] ?? void 0 : e.imageAlt
        };
        v[o].images.push(a);
      });
    else {
      const t = { ...e };
      v[o].images.push(t);
    }
  }
}, me = (e) => {
  v[e].currentIndex--;
}, pe = (e) => {
  v[e].currentIndex++;
}, ge = {
  created(e, o) {
    de(o.value);
  },
  mounted(e, o) {
    const t = () => {
      var r;
      const a = q({
        render() {
          const m = { ...o.value }, y = Array.isArray(o.value.imageUrl) ? { ...o.value, imageUrl: o.value.imageUrl[0] } : { ...o.value }, u = D(m), h = u ? v[u] : void 0;
          if (Array.isArray(m.imageUrl) && (m.imageUrl = m.imageUrl[0]), h) {
            const w = h.images.findIndex((k) => k.imageUrl === y.imageUrl);
            h.currentIndex = w >= 0 ? w : 0;
          }
          return z(ue, {
            ...y,
            currentGallery: h,
            onClose: () => s(a, c),
            onPrevious: () => u !== !1 && me(u),
            onNext: () => u !== !1 && pe(u)
          });
        }
      }), c = document.createElement("div"), d = document.querySelector(((r = o.value) == null ? void 0 : r.anchor) || "body");
      d && (d.appendChild(c), a.mount(c));
    }, s = (a, c) => {
      a.unmount(), c.remove();
    };
    e.style.cursor = "pointer", e.addEventListener("click", t), e.openFullscreenImage = t;
  },
  beforeUnmount(e) {
    e.removeEventListener("click", e.openFullscreenImage);
  }
};
function ye(e) {
  e.directive("fullscreen-image", ge);
}
export {
  ye as fullscreenImagePlugin
};
