export interface FullscreenImage {
  imageUrl: string | string[];
  anchor?: string;
  animation?: 'fade' | 'blur' | 'none';
  imageAlt?: string | string[];
  withDownload?: boolean;
  withClose?: boolean;
  withFocusOnClose?: boolean;
  withCloseOnEscape?: boolean;
  closeOnClikOutside?: boolean;
  maxHeight?: string;
  maxWidth?: string;
  backdropColor?: string;
  gallery?: string | boolean;
}

export interface SingleImage extends FullscreenImage {
  imageUrl: string;
  imageAlt?: string;
}
export interface PanoramaImage extends FullscreenImage {
  imageUrl: string[];
  imageAlt?: string[];
}

export interface FullscreenImageDirectiveConfig extends FullscreenImage {
  imageUrl: string | string[];
  imageAlt?: string | string[];
}

export interface FullscreenImageProps extends SingleImage {
  currentGallery?: FullscreenImageGallery;
}

export interface FullscreenImageGallery {
  currentIndex: number;
  images: SingleImage[];
}

export interface FullscreenImageGalleryMap {
  [key: string]: FullscreenImageGallery;
}
