import { createApp } from 'vue'
import App from './App.vue'
import { fullscreenImagePlugin } from './fullscreen-image-plugin'

createApp(App).use(fullscreenImagePlugin).mount('#app')
