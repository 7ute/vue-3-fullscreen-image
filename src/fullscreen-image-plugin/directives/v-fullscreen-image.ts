import { DirectiveBinding, createApp, h, reactive } from 'vue';
import FullscreenImage from '../component/FullscreenImage.vue';
import { FullscreenImageGalleryMap, FullscreenImageDirectiveConfig, SingleImage } from '../types';

export interface HtmlElementWithMethod extends HTMLElement {
  openFullscreenImage: () => any
}

const GLOBAL_GALLERY_KEY = '_global_gallery'

export const galleryMap: FullscreenImageGalleryMap = reactive({})
export const getGalleryKey = (image: FullscreenImageDirectiveConfig|SingleImage) => {
  if (Array.isArray(image.imageUrl)) {
    return GLOBAL_GALLERY_KEY
  }
  if ((image.gallery ?? false) === false) {
    return false
  }
  return typeof image.gallery === 'string' ? image.gallery : GLOBAL_GALLERY_KEY
}
export const maybeAddImageToItsGallery = (directiveConfig: FullscreenImageDirectiveConfig) => {
  const galleryMapKey = getGalleryKey(directiveConfig)
  if (!galleryMapKey) {
    return
  }
  if (! (galleryMap[galleryMapKey] ?? false)) {
    const newImageGallery = {
        currentIndex: 0,
        images: [],
    }
    galleryMap[galleryMapKey] = newImageGallery
  }
  if (Array.isArray(directiveConfig.imageUrl)) {
    // Backward compatibility: if array of source, add each source as an independent image
    directiveConfig.imageUrl.map((imageUrl, k) => {
      const singleImage: SingleImage = {
        ...directiveConfig,
        imageUrl,
        imageAlt: directiveConfig.imageAlt && Array.isArray(directiveConfig.imageAlt)
            ? (directiveConfig.imageAlt[k] ?? undefined)
            : directiveConfig.imageAlt,
      }
      galleryMap[galleryMapKey].images.push(singleImage)
    })
  } else {
    const singleImage: SingleImage = { ...directiveConfig } as SingleImage
    galleryMap[galleryMapKey].images.push(singleImage)
  }
  
}

export const moveToPreviousImage = (galleryMapKey: string) => {
  galleryMap[galleryMapKey].currentIndex --
}
export const moveToNextImage = (galleryMapKey: string) => {
  galleryMap[galleryMapKey].currentIndex ++
}

export const vFullscreenImagePlugin = {
  created(_: HtmlElementWithMethod, binding: DirectiveBinding<FullscreenImageDirectiveConfig>) {
    maybeAddImageToItsGallery(binding.value)
  },
  mounted(el: HtmlElementWithMethod, binding: DirectiveBinding<FullscreenImageDirectiveConfig>) {
    const openFullscreenImage = () => {
      const app = createApp({
        render() {
          const image = { ...binding.value }
          const singleImage: SingleImage = Array.isArray(binding.value.imageUrl)
            ? { ...binding.value, imageUrl: binding.value.imageUrl[0] } as SingleImage
            : { ...binding.value } as SingleImage
          const galleryMapKey = getGalleryKey(image)
          const currentGallery = galleryMapKey ? galleryMap[galleryMapKey] : undefined
          // Backward compatibility: if array of source, open on the first image
          if (Array.isArray(image.imageUrl)) {
            image.imageUrl = image.imageUrl[0]
          }
          if (currentGallery) {
            const currentIndex = currentGallery.images.findIndex(i => i.imageUrl === singleImage.imageUrl)
            currentGallery.currentIndex = currentIndex >= 0 ? currentIndex : 0
          }
          return h(FullscreenImage, {
            ...singleImage,
            currentGallery,
            onClose: () => closeFullscreenImage(app, container),
            onPrevious: () => (galleryMapKey !== false) && moveToPreviousImage(galleryMapKey),
            onNext: () => (galleryMapKey !== false) && moveToNextImage(galleryMapKey),
          });
        },
      });

      const container = document.createElement('div');
      const anchorElement = document.querySelector(binding.value?.anchor || 'body')
      if (anchorElement) {
        anchorElement.appendChild(container);
        app.mount(container);
      }
    };

    const closeFullscreenImage = (app: any, container: HTMLElement) => {
      // Cleanup and close the modal
      app.unmount();
      container.remove();
    };

    // Add cursor:pointer; style to the element
    el.style.cursor = 'pointer';

    el.addEventListener('click', openFullscreenImage);

    // Store the openFullscreenImage function in a variable accessible during beforeUnmount
    el['openFullscreenImage'] = openFullscreenImage;
  },

  beforeUnmount(el: HtmlElementWithMethod) {
    // Remove the event listener using the stored function reference
    el.removeEventListener('click', el['openFullscreenImage']);
  },
};
